from pymongo import MongoClient
from bson.objectid import ObjectId
import random
import json

client = MongoClient("mongodb://mongo1:27017,mongo2:27018,mongo3:27019/?replicaSet=personalNotes").PersonalNotes
users = client.users
posts = client.posts

get_count = 1000
all_get_count = 200
post_count = 500

users_list = users.find({}, projection={'_id': False})
bound = users_list.count() - 1

for j in range(1000):
	test_post = {
		'title': 'Test Title #',
		'text' : 'Some test text here',
		'access' : 'public'
	}
	test_post['title'] =  'Test Title #%d' % j
	test_post['author_id'] = users_list[random.randint(0, bound)]['id']
	posts.insert_one(test_post)


with open('data', 'w') as data_file:
	for i in range(post_count):
		test_post = {
			'title': 'Test Title #',
			'text' : 'Some test text here',
			'access' : 'public'
		}
		test_post['title'] =  'Test Tank Title #%d' % i
		test_post['author_id'] = users_list[random.randint(0, bound)]['id']
		data_file.write("POST||/api/v1/posts||simple_post||%s\n" % json.dumps(test_post))


posts_id = posts.find({}, projection={'_id':True})
pb = posts_id.count() - 1
with open('data', 'a') as data_file:
	for i in range(get_count):
		data_file.write("GET||/api/v1/posts?post_id=%s||simple_get||\n" % str(posts_id[random.randint(0, pb)]['_id']))
	for i in range(all_get_count):
		data_file.write("GET||/api/v1/posts?author_id=%s||simple_get_all||\n" % str(users_list[random.randint(0, bound)]['id']))

		
		
