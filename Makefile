build:clean
	mkdir out
	g++ --std=c++11 -O2 PersonalNotes/*.cpp -fPIC -lfastcgi-daemon2 -shared -o out/libpersonal_notes.so `pkg-config --cflags --libs libmongocxx` 

clean	:
	rm -rf out

run:
	fastcgi-daemon2 --config=webservice.conf

test:
	echo "sudo docker run -v $(pwd):/var/loadtest -v $HOME/.ssh:/root/.ssh --net host -it direvius/yandex-tank"

mongo:
	sudo docker exec -it mongo1 mongo --eval 'rs.initiate({_id: "personalNotes", members: [{ _id: 0, host: "mongo1:27017"}]})' && sudo docker exec -it mongo1 mongo --eval 'rs.add("mongo2")' && sudo docker exec -it mongo1 mongo --eval  'rs.add("mongo3")'

status:
	sudo docker exec -it mongo1 mongo --eval 'rs.status()'
