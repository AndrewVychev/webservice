#include <fastcgi2/component.h>
#include <fastcgi2/component_factory.h>
#include <fastcgi2/handler.h>
#include <fastcgi2/request.h>
#include <fastcgi2/logger.h>

#include <chrono>

#include <iostream>
#include "AuthRequest.cpp"

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/types.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

thread_local mongocxx::client client;


class PersonalNotesClass : virtual public fastcgi::Component, virtual public fastcgi::Handler {

public:

    PersonalNotesClass(fastcgi::ComponentContext *context) : fastcgi::Component(context) {
    }

    virtual ~PersonalNotesClass() {}

    virtual void onLoad() {
        logger = context()->findComponent<fastcgi::Logger>("daemon-logger");
        if (!logger) {
            throw std::runtime_error("cannot get component");
        }
    }

    virtual void onUnload() {}

    virtual void onThreadStart() {
        mongocxx::uri uri(URI);
        client = mongocxx::client(uri);

    }

    virtual void handleRequest(fastcgi::Request *request, fastcgi::HandlerContext *context) {

        request->setContentType(APPLICATION_JSON);
        if (request->getRequestMethod().compare(POST) == 0) {
            newPost(request);
        } else if (request->getRequestMethod().compare(GET) == 0) {
            if (request->hasArg(POST_ID)) {
                getPost(request);
            } else if (request->hasArg(AUTHOR_ID)) {
                getAllPosts(request);
            } else {
                std::string error = "No query parameters specified";
                request->setStatus(errorStatusCode);
                request->write(error.c_str(), error.length());
            }
        } else if (request->getRequestMethod().compare(DELETE) == 0) {
            removePost(request);
        } else {
            request->sendError(errorNotFound);
        }
    }

private:
    const std::string _ID = "_id";
    const std::string ID = "id";
    const std::string POST_ID = "post_id";
    const std::string AUTHOR_ID = "author_id";
    const std::string POSTS = "posts";
    const std::string ACCESS = "access";
    const std::string TITLE = "title";
    const std::string DATE = "date";
    const std::string TEXT = "text";
    const std::string USERS = "users";
    const std::string URI = "mongodb://mongo1:27017,mongo2:27018,mongo3:27019/?replicaSet=personalNotes&readPreference=primaryPreferred";
    const std::string DB_NAME = "PersonalNotes";

    const std::string POST = "POST";
    const std::string PUT = "PUT";
    const std::string GET = "GET";
    const std::string DELETE = "DELETE";
    const std::string APPLICATION_JSON = "application/json";
    const unsigned short errorStatusCode = 400;
    const unsigned short errorNotFound = 404;
    fastcgi::Logger *logger;

    void newPost(fastcgi::Request *request) {
        mongocxx::database db = client[DB_NAME];
        mongocxx::collection coll = db[POSTS];

        fastcgi::DataBuffer buf = request->requestBody();
        char data[256];
        buf.read(0, data, request->getContentLength());
        std::string json(data);

        std::vector<std::string> validKeys;
        validKeys.push_back(TITLE);
        validKeys.push_back(AUTHOR_ID);
        validKeys.push_back(ACCESS);
        validKeys.push_back(TEXT);

        bsoncxx::document::value doc_value = bsoncxx::from_json(json);

        bsoncxx::builder::stream::document document{};
        getBuilderFromDocumentValue(document, doc_value, validKeys);

        if (jsonValidator(doc_value.view(), validKeys)) {
            document << DATE << bsoncxx::types::b_date {std::chrono::system_clock::now()};
            auto result = coll.insert_one(document.view());
            std::string requestAnswer;
            if (!result) {
                requestAnswer = "Unacknowledged write. No id available.";
                request->write(requestAnswer.c_str(), requestAnswer.length());
                request->setStatus(403);
                return;
            }

            if (result->inserted_id().type() == bsoncxx::type::k_oid) {
                bsoncxx::oid id = result->inserted_id().get_oid().value;
                document << ID << id.to_string();
                requestAnswer = bsoncxx::to_json(document.view());
                request->write(requestAnswer.c_str(), requestAnswer.length());
                request->setStatus(200);
            }
        } else {
            request->setStatus(400);
        }
    }

    void removePost(fastcgi::Request *request) {
        if (!request->hasArg(POST_ID)) {
            std::string error = "No query parameters specified";
            request->setStatus(errorStatusCode);
            request->write(error.c_str(), error.length());
        }

        std::string postId = request->getArg(POST_ID);
        mongocxx::database db = client[DB_NAME];
        mongocxx::collection posts = db[POSTS];

        mongocxx::stdx::optional<mongocxx::result::delete_result> res = posts.delete_one(bsoncxx::builder::stream::document{}
                                               << _ID << bsoncxx::oid(postId)
                                               << bsoncxx::builder::stream::finalize);
        if (res->deleted_count() == 0) {
            std::string requestAnswer = "Error when remove occurred";
            request->write(requestAnswer.c_str(), requestAnswer.length());
            request->setStatus(403);
            return;
        }
    }

    void getBuilderFromDocumentValue(bsoncxx::builder::stream::document &document,
                                     bsoncxx::document::value &doc_value,
                                     std::vector<std::string> &validKeys) {
        for (std::vector<std::string>::iterator it = validKeys.begin(); it != validKeys.end(); it++) {
            document << *it << doc_value.view()[*it].get_utf8().value;
        }
    }

    bool jsonValidator(bsoncxx::document::view view, std::vector<std::string> &validKeys) {
        for (std::vector<std::string>::iterator it = validKeys.begin(); it != validKeys.end(); it++) {
            if (!view[*it]) {
                return false;
            }
        }
        return true;
    }

    void getPost(fastcgi::Request *request) {
    	std::string postId = request->getArg(POST_ID);

        mongocxx::database db = client[DB_NAME];
        mongocxx::collection posts = db[POSTS];

        auto builder = bsoncxx::builder::stream::document{};

        bsoncxx::document::value filter = builder
                << _ID << bsoncxx::oid(postId)
                << bsoncxx::builder::stream::finalize;

        mongocxx::stdx::optional<bsoncxx::document::value> post =
                posts.find_one(filter.view());
        if (post) {
            auto user_post = post.value().view();
            bsoncxx::document::value post_for_return =
                    builder << ID << user_post[_ID].get_oid().value.to_string()
                            << TITLE << user_post[TITLE].get_utf8().value
                            << TEXT << user_post[TEXT].get_utf8().value
                            << ACCESS << user_post[ACCESS].get_utf8().value
                            << AUTHOR_ID << user_post[AUTHOR_ID].get_utf8().value
                            << bsoncxx::builder::stream::finalize;
            std::string result = bsoncxx::to_json(post_for_return.view());
            request->write(result.c_str(), result.length());

        } else {
            std::string error = "Post is not found";
            request->setStatus(errorStatusCode);
            request->write(error.c_str(), error.length());
        }

    }

    void getAllPosts(fastcgi::Request *request) {
    	std::string authorId = request->getArg(AUTHOR_ID);

        mongocxx::database db = client[DB_NAME];
        mongocxx::collection posts = db[POSTS];

        if (getUser(authorId, db).empty()) {
            std::string error = "User is not found";
            request->setStatus(errorStatusCode);
            request->write(error.c_str(), error.length());
            return;
        }

        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::document::value filter = builder
                << AUTHOR_ID << authorId.c_str()
                << bsoncxx::builder::stream::finalize;

        mongocxx::cursor posts_cursor = posts.find(filter.view());

        auto foo = builder << POSTS << bsoncxx::builder::stream::open_array;
        for (auto user_post: posts_cursor) {

            foo << bsoncxx::builder::stream::open_document
                << ID << user_post[_ID].get_oid().value.to_string()
                << TITLE << user_post[TITLE].get_utf8().value
                << TEXT << user_post[TEXT].get_utf8().value
                << ACCESS << user_post[ACCESS].get_utf8().value
                << AUTHOR_ID << user_post[AUTHOR_ID].get_utf8().value
                << bsoncxx::builder::stream::close_document;
        }
        bsoncxx::document::value result = foo << bsoncxx::builder::stream::close_array
                                              << bsoncxx::builder::stream::finalize;

        std::string res = bsoncxx::to_json(result.view());
        request->write(res.c_str(), res.length());

    }

    std::string getUser(std::string userId, mongocxx::database db) {

        mongocxx::collection users = db[USERS];
        auto builder = bsoncxx::builder::stream::document{};

        bsoncxx::document::value filter = builder
                << ID << userId.c_str()
                << bsoncxx::builder::stream::finalize;

        bsoncxx::document::value projection = builder
                << _ID << false
                << bsoncxx::builder::stream::finalize;

        mongocxx::options::find options = mongocxx::options::find();
        options.projection(projection.view());

        mongocxx::stdx::optional<bsoncxx::document::value> user =
                users.find_one(filter.view(), options);
        if (user) {
            std::string result = bsoncxx::to_json(user.value().view());
            return result;
        }
        return "";
    }

};

FCGIDAEMON_REGISTER_FACTORIES_BEGIN()

    FCGIDAEMON_ADD_DEFAULT_FACTORY("auth_factory", AuthRequest)
    FCGIDAEMON_ADD_DEFAULT_FACTORY("personal_notes_factory", PersonalNotesClass)

FCGIDAEMON_REGISTER_FACTORIES_END()

