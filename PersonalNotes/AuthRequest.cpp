#include <fastcgi2/component.h>
#include <fastcgi2/component_factory.h>
#include <fastcgi2/handler.h>
#include <fastcgi2/request.h>
#include <string.h>
#include <iostream>

//
// Created by andrew on 10/14/16.
//

class AuthRequest : virtual public fastcgi::Component, virtual public fastcgi::Handler {

public:

    AuthRequest(fastcgi::ComponentContext *context) : fastcgi::Component(context) {}
    virtual ~AuthRequest() {}
    virtual void onLoad() {}
    virtual void onUnload() {}
    virtual void handleRequest(fastcgi::Request *request, fastcgi::HandlerContext *context) {

        if (request->getRequestMethod().compare("POST") == 0) {

            fastcgi::DataBuffer db = request->requestBody();
            char data[256];
            db.read(0, data, request->getContentLength());
            if (strstr(data, "login") != 0 && strstr(data, "password") != 0) {
                std::string response = "{\n" \
                  "        \"access_token\" : \"u3XOCYV91f2P6odbceNIY_BnkfSpN7gQwzknsRi_.......0iRPlHYNMEES9\",\n" \
                  "        \"expires_in\" : 4000\n" \
                  "}";
                request->write(response.c_str(), response.length());
            }
            else
                request->setStatus(401);
        } else
            request->setStatus(400);
    }

private:
    struct AUTH_BODY {
        std::string login;
        std::string password;
    };

};
